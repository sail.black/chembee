.. chembee documentation master file, created by
   sphinx-quickstart on Sat Aug 20 20:11:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to chembee's documentation!
===================================

`Chembee` is a modelling kit automatizing the first step of the MLOps value pipeline for a given dataset. The perspective is not algorithm-specific but rather 
datacentric. `Chembee` therefore operates data-centric as in contrast to `scikit-learn` that is algorithm centric. 

In the end, data creates value. The package shall help finding the best treatment for a given dataset fast. Automatizing rapid prototyping for environmental degradation modelling and other endpoints, the package merges CADD and Environmental Sciences. 

Models crafted with and by `chembee` must follow the REACH and OECD guidelines for QSAR models replacing experiments for environmental and pharmaceutical endpoints. Therefore, the `actions` module provides functionality to comply with the `REACH` and `OECD` standards. 

The goal of `chembee` is thus to provide methods to create explainable, compliant, and production-ready, QSAR models for use in microservices fast.


Source Code!
===================================
Find the source code on `codeberg.org <https://codeberg.org/sail.black/chembee>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
