chembee.datasets package
========================

Submodules
----------

chembee.datasets.BioDegDataSet module
-------------------------------------

.. automodule:: chembee.datasets.BioDegDataSet
   :members:
   :undoc-members:
   :show-inheritance:

chembee.datasets.BreastCancer module
------------------------------------

.. automodule:: chembee.datasets.BreastCancer
   :members:
   :undoc-members:
   :show-inheritance:

chembee.datasets.ChemicalDataSet module
---------------------------------------

.. automodule:: chembee.datasets.ChemicalDataSet
   :members:
   :undoc-members:
   :show-inheritance:

chembee.datasets.DataSet module
-------------------------------

.. automodule:: chembee.datasets.DataSet
   :members:
   :undoc-members:
   :show-inheritance:

chembee.datasets.IrisDataSet module
-----------------------------------

.. automodule:: chembee.datasets.IrisDataSet
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: chembee.datasets
   :members:
   :undoc-members:
   :show-inheritance:
