chembee.config.calibration package
==================================

Submodules
----------

chembee.config.calibration.kmeans module
----------------------------------------

.. automodule:: chembee.config.calibration.kmeans
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.knn module
-------------------------------------

.. automodule:: chembee.config.calibration.knn
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.linear\_regression module
----------------------------------------------------

.. automodule:: chembee.config.calibration.linear_regression
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.logistic\_regression module
------------------------------------------------------

.. automodule:: chembee.config.calibration.logistic_regression
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.mlp\_classifier module
-------------------------------------------------

.. automodule:: chembee.config.calibration.mlp_classifier
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.naive\_bayes module
----------------------------------------------

.. automodule:: chembee.config.calibration.naive_bayes
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.random\_forest module
------------------------------------------------

.. automodule:: chembee.config.calibration.random_forest
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.restricted\_bm module
------------------------------------------------

.. automodule:: chembee.config.calibration.restricted_bm
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.spectral\_clustering module
------------------------------------------------------

.. automodule:: chembee.config.calibration.spectral_clustering
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.svc module
-------------------------------------

.. automodule:: chembee.config.calibration.svc
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.calibration.svc\_poly module
-------------------------------------------

.. automodule:: chembee.config.calibration.svc_poly
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: chembee.config.calibration
   :members:
   :undoc-members:
   :show-inheritance:
