chembee.actions package
=======================

Submodules
----------

chembee.actions.applicability module
------------------------------------

.. automodule:: chembee.actions.applicability
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.benchmark\_algorithms module
--------------------------------------------

.. automodule:: chembee.actions.benchmark_algorithms
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.calibration module
----------------------------------

.. automodule:: chembee.actions.calibration
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.classifier\_fit module
--------------------------------------

.. automodule:: chembee.actions.classifier_fit
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.clf\_list module
--------------------------------

.. automodule:: chembee.actions.clf_list
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.cross\_validation module
----------------------------------------

.. automodule:: chembee.actions.cross_validation
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.evaluation module
---------------------------------

.. automodule:: chembee.actions.evaluation
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.feature\_extraction module
------------------------------------------

.. automodule:: chembee.actions.feature_extraction
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.get\_false\_predictions module
----------------------------------------------

.. automodule:: chembee.actions.get_false_predictions
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.get\_undersampled\_data module
----------------------------------------------

.. automodule:: chembee.actions.get_undersampled_data
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.save\_model module
----------------------------------

.. automodule:: chembee.actions.save_model
   :members:
   :undoc-members:
   :show-inheritance:

chembee.actions.search module
-----------------------------

.. automodule:: chembee.actions.search
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: chembee.actions
   :members:
   :undoc-members:
   :show-inheritance:
