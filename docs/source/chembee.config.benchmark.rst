chembee.config.benchmark package
================================

Submodules
----------

chembee.config.benchmark.BenchmarkAlgorithm module
--------------------------------------------------

.. automodule:: chembee.config.benchmark.BenchmarkAlgorithm
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.algorithms module
------------------------------------------

.. automodule:: chembee.config.benchmark.algorithms
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.grid\_search\_cv module
------------------------------------------------

.. automodule:: chembee.config.benchmark.grid_search_cv
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.kmeans module
--------------------------------------

.. automodule:: chembee.config.benchmark.kmeans
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.knn module
-----------------------------------

.. automodule:: chembee.config.benchmark.knn
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.linear\_regression module
--------------------------------------------------

.. automodule:: chembee.config.benchmark.linear_regression
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.logistic\_regression module
----------------------------------------------------

.. automodule:: chembee.config.benchmark.logistic_regression
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.mlp\_classifier module
-----------------------------------------------

.. automodule:: chembee.config.benchmark.mlp_classifier
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.naive\_bayes module
--------------------------------------------

.. automodule:: chembee.config.benchmark.naive_bayes
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.random\_forest module
----------------------------------------------

.. automodule:: chembee.config.benchmark.random_forest
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.restricted\_bm module
----------------------------------------------

.. automodule:: chembee.config.benchmark.restricted_bm
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.spectral\_clustering module
----------------------------------------------------

.. automodule:: chembee.config.benchmark.spectral_clustering
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.svc module
-----------------------------------

.. automodule:: chembee.config.benchmark.svc
   :members:
   :undoc-members:
   :show-inheritance:

chembee.config.benchmark.svc\_poly module
-----------------------------------------

.. automodule:: chembee.config.benchmark.svc_poly
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: chembee.config.benchmark
   :members:
   :undoc-members:
   :show-inheritance:
