# Note that the standard function is already tested in the fixture file prepare_biodeg.py

import numpy as np
import sys, os

 

from tests.prepare_biodeg import *  # breaking the rules by using star because the fixture import is broken otherise

from actions.get_false_predictions import (
    get_data_impurities,
    get_multi_false_predictions,
)


def test_get_multi_false_predictions(fitted_clf, full_data):

    fitted_clf = fitted_clf
    X_data, y_true = full_data
    n = 2
    false_pos_ind, false_neg_ind = get_multi_false_predictions(
        clf=fitted_clf, X_data=X_data, y_data=y_true, n=n
    )
    assert type(false_pos_ind) == type(false_neg_ind) == type([])


def test_get_data_impurities(fitted_clf, full_data):

    fitted_clf = fitted_clf
    X_data, y_true = full_data

    impurites = get_data_impurities(fitted_clf=fitted_clf, X_data=X_data, y_true=y_true)
    assert len(impurites) == 2
    assert type(impurites) == type(impurites[0]) == type(impurites[1]) == type([])
