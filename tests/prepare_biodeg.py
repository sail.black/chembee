# file to prepare the fixtures for other tests. The aim is to refactor them.
import numpy as np
import sys, os
import pytest

 


from chembee.datasets.BioDegDataSet import BioDegDataSet
from chembee.preparation.processing import (
    calculate_lipinski_desc,
    get_mols_from_supplier,
)
from chembee.config.calibration.random_forest import RandomForestClassifierAlgorithm
from chembee.actions.get_false_predictions import get_false_predictions


@pytest.fixture(scope="session")
def data_set():
    prefix = "tests/data"
    target = "ReadyBiodegradability"
    split_ratio = 0.7
    DataSet = BioDegDataSet(
        split_ratio=split_ratio, data_set_path="tests/data/Biodeg.sdf", target=target
    )
    DataSet.data = calculate_lipinski_desc(DataSet.data, DataSet.mols)
    data = calculate_lipinski_desc(DataSet.data, DataSet.mols)
    data = DataSet.clean_data(data.head(100))
    (
        DataSet.X_train,
        DataSet.X_test,
        DataSet.y_train,
        DataSet.y_test,
    ) = DataSet.make_train_test_split(
        data, split_ratio=split_ratio, y_col=target, shuffle=True
    )
    DataSet.data = DataSet.clean_data(DataSet.data)
    return DataSet


@pytest.fixture(scope="session")
def fitted_clf(data_set):
    """
    The fitted_clf function fits a Random Forest Classifier to the data in the
    data_set argument. It returns a reference to that classifier.

    :param data_set: Used to Pass the data set that is used to train and test the model.
    :return: The classifier object, clf.

    :doc-author: Trelent
    """

    rf = RandomForestClassifierAlgorithm()
    clf = rf.fit(X=data_set.X_train.to_numpy(), y=data_set.y_train.astype(np.int32))
    return clf


@pytest.fixture(scope="session")
def false_pred_indices(data_set, fitted_clf):

    clf = fitted_clf
    false_pos, false_neg = get_false_predictions(
        fitted_clf=clf,
        X_data=data_set.X_test.to_numpy(),
        y_true=data_set.y_test.astype(np.int32),
    )
    assert type(false_pos) == type(false_neg) == type([])
    if len(false_pos) == 0 or len(false_neg) == 0:
        # make the subsequent test run
        false_pos = [0]
        false_neg = [0]
    return false_pos, false_neg


@pytest.fixture(scope="session")
def false_pred(data_set, fitted_clf, false_pred_indices):
    """
    The false_pred function returns a list of the molecules that were incorrectly predicted by the model.
    The function takes two arguments:
        1) The data set object (which contains both training and test sets)
        2) The fitted classifier object

    :param data_set: Used to Get the test set from the data_set object.
    :param fitted_clf: Used to Pass the classifier that has been fitted to the training data.
    :return: A list of the indices of the molecules that were incorrectly classified.

    :doc-author: Trelent
    """

    clf = fitted_clf
    false_pos, false_neg = false_pred_indices
    false_pos_mols = get_mols_from_supplier(supplier=data_set.mols, indices=false_pos)
    false_neg_mols = get_mols_from_supplier(supplier=data_set.mols, indices=false_neg)
    assert type(false_pos_mols) == type([])
    assert type(false_neg_mols) == type([])
    return (false_pos_mols, false_neg_mols)


@pytest.fixture(scope="session")
def base_mols(data_set):
    """
    The base_mols function takes a data set and returns the molecules in that
    data set.  It is used to create a list of all the molecules in an input file,
    which are then used to calculate descriptors for each molecule.

    :param data_set: Used to get the molecules from the data_set.
    :return: A list of the molecules in a data set.

    :doc-author: Julian M. Kleber
    """

    mols = data_set.mols
    mols = get_mols_from_supplier(supplier=mols, indices=[i for i in range(len(mols))])
    return mols


@pytest.fixture(scope="session")
def full_data(data_set):
    """
    The full_data function takes a data set as input and returns the following:
        1. X_data, which is the data without the target column
        2. y_data, which is just the target column of our original dataset

    :param data_set: Used to Specify the data set that will be used.
    :return: The x_data and y_data.

    :doc-author: Julian M. Kleber
    """

    target = "ReadyBiodegradability"
    X_data = data_set.data.drop(columns=[target]).to_numpy()
    y_data = data_set.data[target].to_numpy().astype(np.int32)
    return X_data, y_data
