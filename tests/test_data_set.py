import sys, os
import numpy as np

 

from chembee.datasets.BreastCancer import BreastCancerDataset


def test_breast_cancer_dataset():

    DataSet = BreastCancerDataset(split_ratio=0.8)
    assert len(DataSet.X_train) == int(0.8 * len(DataSet.data.data))
    assert DataSet.data is not None
    DataSet.save_data_npy(
        DataSet.data.data, file_name="test_breast_cancer_save", prefix="tests/data/"
    )
    np.load("tests/data/test_breast_cancer_save.npy")
    X_train, X_test, y_train, y_test = DataSet.make_train_test_split(
        DataSet.data, split_ratio=0.8
    )
