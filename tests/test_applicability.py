import sys, os

import numpy as np

 
from chembee.utils.file_utils import save_json_to_file
from chembee.actions.applicability import get_applicability_domain

from chembee.config.calibration.random_forest import RandomForestClassifier
from chembee.config.calibration.knn import KNNClassifier
from chembee.config.calibration.mlp_classifier import NeuralNetworkClassifierRELU
from chembee.config.calibration.svc import NaivelyCalibratedSVCRBF

from chembee.plotting.applicability import plot_applicability_domain

from chembee.utils.file_utils import make_full_filename, save_json_to_file

from tests.prepare_biodeg import data_set


def test_applicabilty(data_set):

    file_name = "test_applicability.json"
    prefix = "tests/plots"
    target = "ReadyBiodegradability"
    split_ratio = 0.7
    data_set = data_set
    interval = (0, 1.05, 0.05)
    clf_list = [
        RandomForestClassifier,
        KNNClassifier,
        NeuralNetworkClassifierRELU,
        NaivelyCalibratedSVCRBF,
    ]
    clf_result = []
    for clf in clf_list:
        clf.fit(data_set.X_train.to_numpy(), data_set.y_train.to_numpy())
        clf_result.append(clf)
    result = {}
    for fitted_clf in clf_result:
        clf_res = get_applicability_domain(
            clf=fitted_clf,
            X_train=data_set.X_train.to_numpy(),
            X_test=data_set.X_test.to_numpy(),
            y_train=data_set.y_train.to_numpy().astype(np.int32),
            y_test=data_set.y_test.to_numpy().astype(np.int32),
            interval=interval,
            similarity_metric="tanimoto",
            metric_evaluation="auc",
        )
        result[fitted_clf.name] = clf_res
    file_name = make_full_filename(prefix=prefix, file_name=file_name)
    save_json_to_file(result, file_name=file_name)
    file_name = "test_appicability.png"
    file_name = make_full_filename(prefix, file_name)
    plot_applicability_domain(clf_res, interval, file_name)
