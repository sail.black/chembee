import sys, os
from sklearn import datasets

 

from chembee.utils.utils import prepare_data_decicion_lib
from chembee.actions.benchmark_algorithms import (
    benchmark_standard,
    benchmark_cv_algorithms,
)
from actions.cross_validation import screen_cross_validation_grid_search

from chembee.config.calibration.random_forest import (
    RandomForestClassifier,
    RandomForestClassifierAlgorithm,
)
from chembee.config.calibration.knn import KNNClassifier, KNeighborsClassifierAlgorithm
from chembee.config.calibration.svc import NaivelyCalibratedSVC, NaivelyCalibratedSVCRBF
from chembee.config.benchmark.logistic_regression import LogisticRegressionClassifier
from chembee.datasets.IrisDataSet import IrisDataSet

from chembee.config.benchmark.algorithms import algorithms


def test_standard_benchmark():
    # TODO Refactor the whole benchmark module it can be more efficient
    DataSet = IrisDataSet(split_ratio=0.8)
    iris = datasets.load_iris()
    X, y = prepare_data_decicion_lib(iris)
    benchmark_standard(
        X, y, feature_names=iris.feature_names[:2], algorithms=algorithms
    )


def test_benchmarking():

    # Data Definition
    DataSet = IrisDataSet(split_ratio=0.8)
    # Grid search
    scores = ["precision_macro", "recall_macro", "accuracy"]
    clf_list = [KNNClassifier, RandomForestClassifier, NaivelyCalibratedSVCRBF]

    fitted_clf, result_clf = screen_cross_validation_grid_search(
        scores=scores,
        clf_list=clf_list,
        X_train=DataSet.X_train,
        X_test=DataSet.X_test,
        y_train=DataSet.y_train,
        y_test=DataSet.y_test,
        refit="accuracy",
    )
    iris = datasets.load_iris()
    X, y = prepare_data_decicion_lib(iris)
    rf = RandomForestClassifierAlgorithm(**fitted_clf["rfc"]["best_parameters"])
    knn = KNeighborsClassifierAlgorithm(**fitted_clf["knn"]["best_parameters"])
    svc = NaivelyCalibratedSVC(**fitted_clf["rbf_svc"]["best_parameters"])
    algorithms = [rf, knn, svc]
    names = ["rf", "knn", "NaivelyCalibratedSVCRBF", "FailedFit"]
    algorithms = [
        rf,
        knn,
        svc,
        None,
    ]  # added none for the case that a classifier could not be fitted
    benchmark_cv_algorithms(
        algorithms=algorithms,
        names=names,
        X=X,
        y=y,
        feature_names=iris.feature_names[:2],
        to_fit=True,
    )
