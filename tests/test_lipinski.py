import sys, os


 

from chembee.plotting.lipinski import polar_plot
from chembee.preparation.processing import calculate_lipinski_desc
from chembee.datasets.BioDegDataSet import BioDegDataSet


def test_polar_plot():
    """
    The test_polar_plot function loads a data file and plots it in polar coordinates.

    The test_polar_plot function loads the Biodeg.sdf dataset from the data directory,
    and then creates a polar plot of each molecule's HBA/HBD ratio on a polar coordinate system.
    This is accomplished by using matplotlib's PolarAxes to create the plot, which is then saved as an image file in the output directory.

    :param script_loc: Used to Specify the location of the script to be tested.
    :return: A list of the dataframe and a figure.

    :doc-author: Trelent
    """

    file_name = "test"
    prefix = "tests/plots"
    target = "ReadyBiodegradability"
    DataSet = BioDegDataSet(
        split_ratio=0.7, data_set_path="tests/data/Biodeg.sdf", target=target
    )
    DataSet.data = calculate_lipinski_desc(DataSet.data.head(100), DataSet.mols)
    polar_plot(DataSet, file_name=file_name, prefix=prefix)
