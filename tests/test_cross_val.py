import sys, os

 

from actions.cross_validation import (
    screen_cross_validation_grid_search,
    stratified_n_fold_filter,
    stratified_n_fold,
)
from chembee.config.calibration.random_forest import RandomForestClassifier
from chembee.config.calibration.knn import KNNClassifier

from tests.prepare_biodeg import data_set, full_data

from chembee.utils.file_utils import make_full_filename, save_json_to_file


def test_naive_cross_validation_filter(full_data):

    X_data, y_data = full_data
    result = stratified_n_fold_filter(
        n=2,
        X_data=X_data,
        y_data=y_data,
        clf=RandomForestClassifier,
        cut_off_filter=0.6,
    )
    keys = list(result.keys())
    assert type(result) == type({})
    assert len(keys) == 3
    assert result[keys[0]] != None
    assert result[keys[1]] != None


def test_naive_cross_validation(full_data):

    X_data, y_data = full_data
    result = stratified_n_fold(
        n=2,
        X_data=X_data,
        y_data=y_data,
        clf=RandomForestClassifier,
        cut_off_filter=0.6,
    )
    keys = list(result.keys())
    assert type(result) == type({})
    assert len(keys) == 2
    assert result[keys[0]] != None
    assert result[keys[1]] != None


def test_grid_search_cross_validation(data_set):

    """
    The benchmark tests cross validation on the random forest classifier based on
    parameters defined in the configuration file of the algorithm
    """

    file_name = "test"
    prefix = "tests/plots"
    scores = [
        "precision_macro",
        "recall_macro",
        "accuracy",
    ]  # need to specify the loss functions for the crosss validation
    data_set = data_set
    names = ["rf", "knn"]
    clf_list = [KNNClassifier, RandomForestClassifier]

    fitted_clf, result_clf = screen_cross_validation_grid_search(
        scores=scores,
        clf_list=clf_list,
        X_train=data_set.X_train,
        X_test=data_set.X_test,
        y_train=data_set.y_train,
        y_test=data_set.y_test,
        refit="accuracy",
    )
    file_name = make_full_filename(
        prefix="tests/data/", file_name="test_cross_validation"
    )
    save_json_to_file(fitted_clf, file_name=file_name)
