import sys, os

 

from chembee.datasets.BioDegDataSet import BioDegDataSet
from actions.feature_extraction import (
    get_feature_importances,
    filter_importance_by_std,
)
from chembee.preparation.processing import calculate_lipinski_desc
from chembee.plotting.feature_extraction import plot_feature_importances

from chembee.utils.file_utils import save_json_to_file, make_full_filename


def test_feature_extraction():

    file_name = "test_feature_extraction"
    prefix = "tests/plots"
    target = "ReadyBiodegradability"
    DataSet = BioDegDataSet(
        split_ratio=0.7, data_set_path="tests/data/Biodeg.sdf", target=target
    )
    data = calculate_lipinski_desc(DataSet.data, DataSet.mols)
    data = DataSet.clean_data(DataSet.data)
    DataSet.data = data
    file_name_save = make_full_filename(prefix="tests/data", file_name="test_save.csv")
    DataSet.save_data_csv(data, file_name=file_name_save)
    X_data = data.drop(columns=target).to_numpy()
    y_data = data[target].to_numpy()
    result_json = get_feature_importances(
        X_data, y_data, feature_names=data.drop(columns=target).columns.to_list()
    )
    file_name_json = make_full_filename(
        file_name="test_feature_importance.json", prefix="tests/data/"
    )
    save_json_to_file(result_json, file_name=file_name_json)
    plot_feature_importances(result_json, file_name, prefix)
    plot_feature_importances(
        result_json, file_name + "_sparsexaxis", prefix, show_x_label=False
    )
    file_name = make_full_filename(prefix=prefix, file_name=file_name)
    result_json = filter_importance_by_std(result_json, cut_off=0.01)
