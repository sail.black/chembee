import pytest
import sys, os
import numpy as np

 

from chembee.plotting.distribution import plot_data_set_distribution_by_col


from chembee.utils.file_utils import make_full_filename, save_json_to_file

from tests.prepare_biodeg import data_set


def test_plot_plot_data_set_distribution_by_col(data_set):

    prefix = "tests/plots/"
    columns = ["InertialShapeFactor", "LogP", "MolWt"]
    x_labels = ["LogP", "InertialShapeFactor", "MolWt"]
    file_names = [
        prefix + "_lipinski_InertialShapeFactor_dist.png",
        prefix + "_lipinski_LogP_dist.png",
        prefix + "_lipinski_MolWt_dist.png",
    ]
    data_set = data_set
    frame = data_set.data
    for i, col in enumerate(columns):
        x_label = x_labels[i]
        file_name = file_names[i]
        plot_data_set_distribution_by_col(
            frame=frame, column=col, x_label=x_label, file_name=file_name
        )
