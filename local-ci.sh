black chembee/
autopep8 --in-place --recursive chembee/
python -m flake8 chembee/ --count --select=E9,F63,F7,F82 --show-source --statistics
mypy --strict chembee/
python -m pylint -f parseable chembee/*.py 
pytest tests/