from chembee.config.calibration.logistic_regression import LogisticRegressionClassifier
from chembee.config.calibration.naive_bayes import NaiveBayesClassifier
from chembee.config.calibration.svc import NaivelyCalibratedSVCRBF
from chembee.config.calibration.svc import NaivelyCalibratedSVCPolynomial
from chembee.config.calibration.random_forest import RandomForestClassifier
from chembee.config.calibration.knn import KNNClassifier
from chembee.config.calibration.mlp_classifier import NeuralNetworkClassifierRELU
from chembee.config.calibration.mlp_classifier import NeuralNetworkClassifierTanh


clf_list = [
    LogisticRegressionClassifier,
    NaiveBayesClassifier,
    NaivelyCalibratedSVCRBF,
    NaivelyCalibratedSVCPolynomial,
    RandomForestClassifier,
    KNNClassifier,
    NeuralNetworkClassifierRELU,
    NeuralNetworkClassifierTanh,
]
