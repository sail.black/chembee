import seaborn as sns


def plot_data_set_distribution_by_col(frame, column, x_label: str, file_name: str):
    """
    The plot_data_set_distribution_by_col function plots the distribution of a column in a dataframe.
    It takes as input:
    - frame: The dataframe to be plotted.
    - column: The name of the column to be plotted.
    - x_label (optional): A label for the x-axis'.
    - file_name (optional): A string containing a path and file name where you want your plot saved, default is 'plot'

    :param frame: Used to specify the data frame that contains the column to be plotted.
    :param column: Used to secify the column of the dataframe that will be used to create the plot.
    :param x_label:str: Used to set the x-axis label.
    :param file_name:str: Used to specify the name of the file to save.
    :return: A plot of the distribution of a column in a dataframe.

    :doc-author: Julian M. Kleber
    """

    ax = sns.distplot(
        frame[column],
        hist=True,
        kde=True,
        bins=int(180 / 5),
        color="darkblue",
        hist_kws={"edgecolor": "black"},
        kde_kws={"linewidth": 4},
    )
    ax.set(xlabel=x_label)
    fig = ax.get_figure()
    fig.savefig(file_name, dpi=300)
