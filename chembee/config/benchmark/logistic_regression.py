from chembee.config.benchmark.BenchmarkAlgorithm import BenchmarkAlgorithm
from sklearn.linear_model import LogisticRegression
import os
import sys

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..")))


class LogisticRegressionClassifier(BenchmarkAlgorithm):
    name = "logistic-regression"

    algorithms = (
        LogisticRegression(penalty="l1", solver="saga", max_iter=1000),
        LogisticRegression(penalty="l2", solver="saga", max_iter=1000),
        LogisticRegression(
            penalty="elasticnet", solver="saga", l1_ratio=0.5, max_iter=1000
        ),
    )

    titles = (
        "l1",
        "l2",
        "elasticnet with $\chi(\mathrm{l1}) = 0.5$",
    )
