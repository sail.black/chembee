import os
import time

import logging

logging.basicConfig(
    format="%(levelname)s:%(asctime)s %(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
    level=logging.DEBUG,
    filename=os.getenv("LOGFILE"),
)

LOGGER = logging.getLogger(__name__)


timestr = time.strftime("%Y%m%d-%H%M%S")


def prepare_data_decicion_lib(data_set: object, columns: list = None) -> tuple():
    """
    only for scikit learn datasets
    """

    if columns is None:
        X = data_set.data[:, :2]
    else:
        assert (
            len(columns) == 2
        ), "Length of the columns input must be equalt to two. Otherwise the plotting of the decision boundary can't work"
        X = np.zeros((len(data_set.target), 2))
        X[:, 0] = data_set.data[:, columns[0]]
        X[:, 1] = data_set.data[:, columns[1]]
    y = data_set.target
    return (X, y)


def get_grid_positions(rows: int, cols: int):

    grid = []
    for i in range(2, rows):
        for j in range(cols):
            grid.append((i, j))
    return grid
